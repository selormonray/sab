<?php

use App\Claimant;
use Illuminate\Database\Seeder;

class ClaimantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Claimant::truncate();

        $faker = \Faker\Factory::create();

        $password = Hash::make("selorm");

        for ($i = 0; $i < 50; $i++) {

            $claimant_id = strtoupper($faker->lexify('C??') . $faker->numberBetween($min = 100, $max = 900));

            //GT-785-18
            $car_number = strtoupper($faker->lexify('G?')."-".$faker->numberBetween($min = 100, $max = 900)."-".$faker->numberBetween($min = 10, $max = 90));

            Claimant::create([
                'full_name' => $faker->lastName. " " .$faker->firstName,
                'dob' => $faker->date($format = 'Y-m-d'),
                'gender' => $faker->randomElement(array('Male', 'Female')),
                'phone_number' => $faker->e164PhoneNumber,
                'email' => $faker->email,
                'password' => $password,
                'claimant_id'=> $claimant_id,
                'car_number' => $car_number,
            ]);


        }

    }
}
