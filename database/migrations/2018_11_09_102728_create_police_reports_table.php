<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoliceReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('police_reports')) {
            Schema::create('police_reports', function (Blueprint $table) {
                $table->increments('id');
                $table->string('claim_id',20)->nullable();
                $table->string('station_name', 100)->nullable();
                $table->string('location', 100)->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('police_reports');
    }
}
