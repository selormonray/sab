<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrokersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('brokers')) {
            Schema::create('brokers', function (Blueprint $table) {
                $table->increments('id');
                $table->string('broker_id', 20)->unique()->nullable();
                $table->string('name', 100)->nullable();
                $table->string('phone_number', 30)->unique()->nullable();
                $table->string('email', 100)->unique()->nullable();
                $table->text('location')->nullable();
                $table->text('address')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brokers');
    }
}
