<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('claims')) {
            Schema::create('claims', function (Blueprint $table) {
                $table->increments('id');
                $table->string('claim_id',20)->unique()->nullable();
                $table->string('claimant_id',20)->nullable();
                $table->string('claim_type', 50);
                $table->string('policy_number',50)->unique()->nullable();
                $table->string('broker_id',20)->nullable();
                $table->boolean('police_report')->nullable();
                $table->text('details');
                $table->string('status');
                $table->timestamps();

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('claims');
    }
}
