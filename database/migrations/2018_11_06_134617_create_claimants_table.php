<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

       if(!Schema::hasTable('claimants')) {
           Schema::create('claimants', function (Blueprint $table) {
               $table->increments('id');
               $table->string('claimant_id', 20)->unique()->nullable();
               $table->string('full_name', 100);
               $table->date('dob')->nullable();
               $table->string('gender', 10)->nullable();
               $table->string('phone_number', 30)->unique()->nullable();
               $table->string('email', 100)->unique();
               $table->string('car_number', 20)->unique()->nullable();
               $table->timestamp('email_verified_at')->nullable();
               $table->string('occupation', 100)->nullable();
               $table->text('street_address')->nullable();
               $table->text('postal_address')->nullable();
               $table->string('digital_address', 50)->nullable();
               $table->string('identification_type', 50)->nullable();
               $table->string('identification_number', 100)->nullable();
               $table->text('identification_image_path')->nullable();
               $table->string('password', 255);
               $table->boolean('is_verified')->nullable();
               $table->string('api_token', 60)->unique()->nullable();
               $table->text('picture')->nullable();
               $table->string('beneficiary_name', 100)->nullable();
               $table->string('beneficiary_relationship', 50)->nullable();
               $table->string('beneficiary_phone_number', 30)->nullable();
               $table->rememberToken();
               $table->timestamps();
           });
       }


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('claimants');
    }
}
