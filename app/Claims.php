<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Claims extends Model
{
    public function claimant() {
        return $this->belongsTo(Claimant::class, 'claimant_id', 'claimant_id');
    }

    public function broker(){
        return $this->belongsTo(Broker::class, 'broker_id', 'broker_id');
    }

    public function policeInfo(){
        return $this->hasOne(PoliceReport::class, 'claim_id','claim_id');
    }

}
