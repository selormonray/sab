<?php

namespace App\Http\Controllers;

use App\Claimant;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class ClaimantController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
//      Claimant view
    public function getClaimants()
    {
        $claimants = Claimant::all();
        return view('claimant.index', compact('claimants'));
    }


    /**
     * Add a claimant
     *
     * @return \Illuminate\Http\Response
     */
    public function add_claimant()
    {
        $view_claimant = Claimant::all();
        return view('claimant.add', compact('view_claimant'));
    }



    public function saveClaimant(Request $request)
    {

        try {
            $this->validate($request, [
                'full_name' => 'unique:claimants,full_name',
                'email' => 'unique:claimants,email',
                'phone_number' => 'unique:claimants,phone_number',
                'car_number' => 'unique:claimants,car_number',
            ]);
        } catch (ValidationException $e) {
        }

        $claimant = new Claimant();
        $claimant->full_name = $request->full_name;
        $claimant->email = $request->input('email');
        $claimant->phone_number = $request->input('phone_number');
        $claimant->car_number = $request->input('car_number');
        $claimant->password = bcrypt($request->input('password'));
        $claimant->save();
        return redirect()->route('claimants');

    }


//      View Claimant
    public function viewClaimant($id)
    {
        $claimant = Claimant::find($id);

        return view('claimant.view', compact('claimant'));
    }


    public function deleteClaimant(Request $request, $id)
    {
        Claimant::destroy($id);


        $request->session()->flash('status', [
            'error' => false,
            'title' => 'Great!',
            'message' => 'User Deleted.',
        ]);


        return back();
    }

    public function editClaimant($id)
    {
        $claimant = Claimant::find($id);
        return view('claimant.edit', compact('claimant'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateClaimant(Request $request, $id)
    {

        $this->validate($request, [
            'phone_number' => 'required|min:2|string',
            'car_number' => 'required|min:2|string',
        ]);
        $claimant = Claimant::find($id);
        $claimant->full_name = $request->full_name;
        $claimant->phone_number = $request->phone_number;
        $claimant->car_number = $request->car_number;
        $claimant->save();

        $request->session()->flash('status', [
            'error' => false,
            'title' => 'Great!',
            'message' => 'Claimant Updated.',
        ]);

        return redirect()->route('claimants');
    }


}
