<?php

namespace App\Http\Controllers;

use App\Permission;
use App\Role;
use App\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserManagementController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Shows the list of user (10 per page).
     *
     * @return \Illuminate\Http\Response
     */
    public function users()
    {
        $users = User::paginate(10);;
        return view('user-management.users.index', compact('users'));
    }

    /**
     * Add a user
     *
     * @return \Illuminate\Http\Response
     */
    public function add_user()
    {

        $roles = Role::all();

        return view('user-management.users.add', compact('roles'));
    }

    /**
     * Save new a user
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function save_user(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);


//        //We assign a then assign a role and permissions to a user
//        $role = Role::where('name', $request->input('role'))->first();
//        if (!empty($role)) {
//            $user->attachRole($role);
//        }

        $request->session()->flash('status', [
            'error' => false,
            'title' => 'Great!',
            'message' => 'New user added.',
        ]);


        return redirect()->route('manage.users');
    }

    /**
     * Opens the user page.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function view_user($id)
    {
        $user = User::find($id);
        return view('user-management.users.view', compact('user'));
    }

    /**
     * Opens the edit user page.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit_user($id)
    {
        $user = User::find($id);
        return view('user-management.users.edit', compact('user'));
    }

    /**
     * Delete User Account.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function delete_user(Request $request, $id)
    {
        User::destroy($id);


        $request->session()->flash('status', [
            'error' => false,
            'title' => 'Great!',
            'message' => 'User Deleted.',
        ]);

        return back();
    }

    /**
     * Updates user information.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update_user(Request $request, $id)
    {

        $this->validate($request, [
            'name' => 'required|min:2|string',
            'email' => 'required|min:2|email|unique:users,email,' . $id,
        ]);

        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();

        $request->session()->flash('status', [
            'error' => false,
            'title' => 'Great!',
            'message' => 'User Updated.',
        ]);

        return redirect()->route('manage.users');
    }

//    ****************PERMISSIONS*******************

    /**
     * List all the permissions.
     *
     * @return \Illuminate\Http\Response
     */
    public function permissions()
    {
        $permissions = Permission::paginate(10);
        return view('user-management.permissions.index', compact('permissions'));
    }

    /**
     * Opens the permission information page.
     *
     * @return \Illuminate\Http\Response
     */
    public function view_permission($id)
    {
        $permission = Permission::find($id);
        return view('user-management.permissions.view', compact('permission'));
    }

    /**
     * Create permissions.
     *
     * @return \Illuminate\Http\Response
     */
    public function add_permission()
    {

        return view('user-management.permissions.add');
    }

    /**
     * List all the permissions.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */

    public function save_permission(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:permissions,name|string|min:3',
            'display_name' => 'nullable|string|min:3',
            'description' => 'nullable|string|min:3'
        ]);
        try {
            $createPermission = new Permission();
            $createPermission->name = str_slug(strtolower($request->name));
            $createPermission->display_name = $request->display_name; // optional
            $createPermission->description = $request->description; // optional
            $createPermission->save();

            $request->session()->flash('status', [
                'error' => false,
                'title' => 'Great!',
                'message' => 'New permission added.',
            ]);

            return redirect()->route('manage.permissions');


        } catch (QueryException $ex) {

            $request->session()->flash('status', [
                'error' => true,
                'title' => 'Sorry!',
                'message' => 'Issue adding permission.',
            ]);

            return back();
        }
    }

    /**
     * Edit a permissions.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function edit_permission($id)
    {
        $permission = Permission::find($id);
        return view('user-management.permissions.edit', compact('permission'));
    }

    /**
     * Update a permissions.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update_permission(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|unique:permissions,name,' . $id . '|string|min:3',
            'display_name' => 'nullable|string|min:3',
            'description' => 'nullable|string|min:3'
        ]);

        try {
            $updatePermission = Permission::find($id);
            $updatePermission->name = str_slug(strtolower($request->name));
            $updatePermission->display_name = $request->display_name; // optional
            $updatePermission->description = $request->description; // optional
            $updatePermission->save();


            $request->session()->flash('status', [
                'error' => false,
                'title' => 'Great!',
                'message' => 'Permission updated.',
            ]);

            return redirect()->route('manage.permissions');
        } catch (QueryException $ex) {

            $request->session()->flash('status', [
                'error' => true,
                'title' => 'Sorry!',
                'message' => 'Issue updating permission.',
            ]);

            return back();
        }
    }

    /**
     * Delete Permission.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function delete_permission(Request $request, $id)
    {
        Permission::destroy($id);

        $request->session()->flash('status', [
            'error' => false,
            'title' => 'Great!',
            'message' => 'Permission Deleted!',
        ]);

        return back();
    }



    //********************ROLES*******************************

    /**
     * All roles page.
     *
     * @return \Illuminate\Http\Response
     */
    public function roles()
    {
        $roles = Role::paginate(10);
        return view('user-management.roles.index', compact('roles'));
    }

    /**
     * View role details.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function view_role($id)
    {
        $role = Role::find($id);

        return view('user-management.roles.view', compact('role'));
    }

    /**
     * Add a new role page.
     *
     * @return \Illuminate\Http\Response
     */
    public function add_role()
    {
        $permissions = Permission::all();
        return view('user-management.roles.add', compact('permissions'));
    }

    /**
     * Save new role details.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function save_role(Request $request)
    {
//    return $request->all();
        $request->name = str_slug(strtolower($request->name));

        $this->validate($request, [
            'name' => 'required|string|min:2|unique:roles,name',
            'display_name' => 'nullable|string|min:2',
            'description' => 'nullable|string|min:3',
            'permissions' => 'required|array|min:1',
            'permissions.*' => 'required|numeric|min:1'
        ]);

        try {
            $owner = new Role();
            $owner->name = $request->name;
            $owner->display_name = $request->display_name;
            $owner->description = $request->description;
            $owner->save();

            $owner->attachPermissions($request->permissions);

            $request->session()->flash('status', [
                'error' => false,
                'title' => 'Great!',
                'message' => 'New role added!',
            ]);

            return redirect()->route('manage.roles');
        } catch (QueryException $ex) {

            $request->session()->flash('status', [
                'error' => true,
                'title' => 'Sorry!',
                'message' => 'Issue adding role.',
            ]);

            return back();
        }

    }

    /**
     * Edit role page.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit_role($id)
    {
        $role = Role::find($id);
        $role_perms = $role->permissions;
        $role_permissions = array();

        foreach ($role_perms as $role_perm) {
            $role_permissions[] = $role_perm->id;
        }

        $permissions = Permission::all();

        return view('user-management.roles.edit', compact('role', 'permissions', 'role_permissions'));
    }

    /**
     * Updates the role details.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update_role(Request $request, $id)
    {
        //creates a slug name
        $request->name = str_slug(strtolower($request->name));

        $this->validate($request, [
            'name' => 'required|string|min:3|unique:roles,name,' . $id,
            'display_name' => 'nullable|string|min:3',
            'description' => 'nullable|string|min:3',
            'permissions' => 'required|array|min:1',
            'permissions.*' => 'required|numeric|min:1'
        ]);

        try {
            $owner = Role::find($id);
            $owner->name = str_slug(strtolower($request->name));
            $owner->display_name = $request->display_name;
            $owner->description = $request->description;
            $owner->save();

            $owner->syncPermissions($request->permissions);

            $request->session()->flash('status', [
                'error' => false,
                'title' => 'Great!',
                'message' => 'Role Updated!',
            ]);

            return redirect()->route('manage.roles');

        } catch (QueryException $exception) {

            $request->session()->flash('status', [
                'error' => true,
                'title' => 'Sorry!',
                'message' => 'Issue updating role.',
            ]);

            return back();
        }
    }

    /**
     * Delete Permission.
     *
     * @return \Illuminate\Http\Response
     */
    public function delete_role(Request $request, $id)
    {
        Role::destroy($id);

        $request->session()->flash('status', [
            'error' => false,
            'title' => 'Great!',
            'message' => 'Role Deleted!',
        ]);

        return back();
    }



    //************************User Permissions and Roles************************

    /**
     * Set User Permissions
     * @param $user_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function permissions_user($user_id)
    {
        $user = User::find($user_id);
        $user_perms = $user->permissions;
        $user_permissions = array();

        foreach ($user_perms as $user_perm) {
            $user_permissions[] = $user_perm->id;
        }
        $permissions = Permission::all();
        return view('user-management.users.permissions', compact('user', 'permissions', 'user_permissions'));
    }

    /**
     * Save User Permissions
     * @param Request $request
     * @param $user_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save_permissions_user(Request $request, $user_id)
    {
//        return $request->all();
        $user = User::find($user_id);

        $user->syncPermissions($request->permissions);

        $request->session()->flash('status', [
            'error' => false,
            'title' => 'Great!',
            'message' => 'User permissions updated',
        ]);

        return redirect()->route('manage.user.view', [$user_id]);
    }

    /**
     * Set User Permissions
     */
    public function roles_user($user_id)
    {
        $user = User::find($user_id);
        $user_rs = $user->roles;
        $user_roles = array();

        foreach ($user_rs as $user_r) {
            $user_roles[] = $user_r->id;
        }
        $roles = Role::all();

        return view('user-management.users.roles', compact('user', 'roles', 'user_roles'));
    }

    /**
     * Save User Permissions
     * @param Request $request
     * @param $user_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save_roles_user(Request $request, $user_id)
    {
//        return $request->roles;
        $user = User::find($user_id);

        $user->syncRoles($request->roles);

        $request->session()->flash('status', [
            'error' => false,
            'title' => 'Great!',
            'message' => 'User roles updated',
        ]);

        return redirect()->route('manage.user.view', [$user_id]);
    }


}
