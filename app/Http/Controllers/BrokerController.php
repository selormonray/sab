<?php

namespace App\Http\Controllers;

use App\Broker;
use DOMDocument;
use Illuminate\Http\Request;

class BrokerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

//  Broker View
    public function getBrokers()
    {
        $broker = Broker::all();
        return view('broker.index', compact('broker'));
    }

    public function addBrokers()
    {
        $view_broker = Broker::all();
        return view('broker.add', compact('view_broker'));
    }




    public function saveBrokers(Request $request)
    {

        $this->validate($request, [
            'phone_number' => 'unique:brokers,phone_number',
            'email' => 'unique:brokers,email',
        ]);

        $brokers = new Broker();
        $brokers->name = $request->input('name');
        $brokers->phone_number = $request->input('phone_number');
        $brokers->email = $request->input('email');
        $brokers->location = $request->input('location');
        $brokers->address = $request->input('address');
        $brokers->save();
        return redirect()->route('brokers');
    }

//      View Broker
    public function viewBrokers($id)
    {
        $brokers = Broker::find($id);

        return view('broker.view', compact('brokers'));
    }


//    Delete Broker

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteBrokers(Request $request, $id)
    {
        Broker::destroy($id);
        $request->session()->flash('status', [
            'error' => false,
            'title' => 'Great!',
            'message' => 'Broker Deleted.',
        ]);
        return back();
    }
    public function editBrokers($id)
    {
        $broker = Broker::find($id);
        return view('broker.edit', compact('broker'));
    }
    public function updateBrokers(Request $request, $id)
    {

        $this->validate($request, [
            'email' => 'required|min:2|email|unique:users,email,' . $id,
        ]);

        $broker = Broker::find($id);

        $broker->email = $request->email;
        $broker->name = $request->name;
        $broker->phone_number = $request->phone_number;
        $broker->location = $request->location;
        $broker->address = $request->address;
        $broker->save();

        $request->session()->flash('status', [
            'error' => false,
            'title' => 'Great!',
            'message' => 'Broker Updated.',
        ]);

        return redirect()->route('brokers');
    }


    public function scrapeWeb() {

//
//        $sample = nl2br("No. 32 Farrar Avenue
//Sikkens Paint Building
//Adabraka, Accra
//Tel: 020 2018135");
//        print_r(explode('\n',$sample,'5'));


        $htmlContent = file_get_contents("http://nicgh.org/intermediaries/brokers/");

        $dom = new DOMDocument();
        @$dom->loadHTML($htmlContent);


        $Header = $dom->getElementsByTagName('th');
        $Detail = $dom->getElementsByTagName('td');

        foreach($Header as $NodeHeader)
        {
            $aDataTableHeaderHTML[] = trim($NodeHeader->textContent);
        }
//        print_r($aDataTableHeaderHTML); die();
        //#Get row data/detail table without header name as key
        $i = 0;
        $j = 0;
        foreach($Detail as $sNodeDetail)
        {
            $aDataTableDetailHTML[$j][] = trim($sNodeDetail->textContent);
            $i = $i + 1;
            $j = $i % count($aDataTableHeaderHTML) == 0 ? $j + 1 : $j;
        }


        $i = 0;
        foreach ($aDataTableDetailHTML as $data) {

            $exploded_data = explode('<br />',nl2br($data[2]),'5');

            $broker = new Broker();
            $broker->broker_id = "BROID".$i++;
            $broker->name = $data[0];
            $broker->phone_number = $exploded_data[1];
            $broker->email = $exploded_data[2];
            $broker->location = $exploded_data[3];
            $broker->address = $exploded_data[0];
            $broker->save();

        }

        dd($i);
    }





}
