<?php

namespace App\Http\Controllers;

use App\Broker;
use App\Claims;
use DB;
use Illuminate\Http\Request;

class ClaimsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

//     get Claims
    public function getClaims()
    {
        $claims = Claims::with('claimant')
            ->with('broker')
            ->with('policeInfo')->get();


        return view('claims.index', compact('claims'));
    }


    public function processClaim(Request $request) {
        $id = $request->input('id');

        $claim = Claims::where('id', $id)->first();

        if (!empty($claim)) {
            $claim->status = "processing";
            $claim->save();
        }
        return back();
    }



    public function approveClaim(Request $request){
        $id = $request->input('id');
        $claim = Claims::where('id', $id)->first();
        if (!empty($claim)){
            $claim->status = "approved";
            $claim->save();
        }
        return back();
    }

    public function declineClaim(Request $request){
        $id = $request->input('id');
        $claim = Claims::where('id', $id)->first();
        if (!empty($claim)){
            $claim->status = "declined";
            $claim->save();
        }
        return back();
    }
//      View Claim
    public function viewClaim($id)
    {
        $claim = Claims::find($id);

        return view('claims.view', compact('claim'));
    }

}
