<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::name('manage.')->group(function () {
    Route::prefix('manage')->group(function () {
        //Users Routes
        Route::get('users', 'UserManagementController@users')->name('users');
        Route::get('users/add', 'UserManagementController@add_user')->name('user.add');
        Route::post('users/save', 'UserManagementController@save_user')->name('user.save');
        Route::get('user/{id}', 'UserManagementController@view_user')->name('user.view');
        Route::get('user/{id}/edit', 'UserManagementController@edit_user')->name('user.edit');
        Route::put('user/{id}/update', 'UserManagementController@update_user')->name('user.update');
        Route::delete('user/{id}/delete', 'UserManagementController@delete_user')->name('user.delete');
        Route::get('user/{id}/permissions', 'UserManagementController@permissions_user')->name('user.permissions');
        Route::get('user/{id}/roles', 'UserManagementController@roles_user')->name('user.roles');
        Route::post('user/{id}/permissions', 'UserManagementController@save_permissions_user')->name('user.permissions.save');
        Route::post('user/{id}/roles', 'UserManagementController@save_roles_user')->name('user.roles.save');

        //Permissions Routes
        Route::get('permissions', 'UserManagementController@permissions')->name('permissions');
        Route::get('permission/add', 'UserManagementController@add_permission')->name('permission.add');
        Route::get('permission/{id}', 'UserManagementController@view_permission')->name('permission.view');
        Route::post('permission/add', 'UserManagementController@save_permission')->name('permission.save');
        Route::get('permission/{id}/edit', 'UserManagementController@edit_permission')->name('permission.edit');
        Route::put('permission/{id}/update', 'UserManagementController@update_permission')->name('permission.update');
        Route::delete('permission/{id}/delete', 'UserManagementController@delete_permission')->name('permission.delete');

        //Roles Routes
        Route::get('roles', 'UserManagementController@roles')->name('roles');
        Route::get('role/add', 'UserManagementController@add_role')->name('role.add');
        Route::get('role/{id}', 'UserManagementController@view_role')->name('role.view');
        Route::post('role/add', 'UserManagementController@save_role')->name('role.save');
        Route::get('role/{id}/edit', 'UserManagementController@edit_role')->name('role.edit');
        Route::put('role/{id}/update', 'UserManagementController@update_role')->name('role.update');
        Route::delete('role/{id}/delete', 'UserManagementController@delete_role')->name('role.delete');

    });


});
//
//Claimants Routes
Route::get('claimants', 'ClaimantController@getClaimants')->name('claimants');
Route::get('claimants/add', 'ClaimantController@add_claimant')->name('claimant.add');
Route::post('claimants/save', 'ClaimantController@saveClaimant')->name('claimant.save');
Route::get('claimants/{id}', 'ClaimantController@viewClaimant')->name('claimant.view');
Route::delete('claimants/{id}/delete', 'ClaimantController@deleteClaimant')->name('claimant.delete');
Route::get('claimants/{id}/edit', 'ClaimantController@editClaimant')->name('claimant.edit');
Route::put('claimants/{id}/update', 'ClaimantController@updateClaimant')->name('claimant.update');

//      Claims Route
Route::get('claims', 'ClaimsController@getClaims')->name('claims');
Route::post('claims', 'ClaimsController@updateStatus')->name('update_claim_status');
Route::post('process-claims', 'ClaimsController@processClaim')->name('process-claim');
Route::post('approve-claims', 'ClaimsController@approveClaim')->name('approve-claim');
Route::post('decline-claims', 'ClaimsController@declineClaim')->name('decline-claim');
Route::get('claims/{id}', 'ClaimsController@viewClaim')->name('claims.view');

//Broker Route
Route::get('broker', 'BrokerController@getBrokers')->name('brokers');
Route::get('broker/add', 'BrokerController@addBrokers')->name('broker.add');
Route::post('broker/add', 'BrokerController@saveBrokers')->name('broker.save');
Route::get('broker/{id}', 'BrokerController@viewBrokers')->name('broker.view');
Route::delete('broker/{id}/delete', 'BrokerController@deleteBrokers')->name('broker.delete');
Route::get('broker/{id}/edit', 'BrokerController@editBrokers')->name('broker.edit');
Route::put('broker/{id}/update', 'BrokerController@updateBrokers')->name('broker.update');


Route::get('scrape', 'BrokerController@scrapeWeb')->name('scrape');

