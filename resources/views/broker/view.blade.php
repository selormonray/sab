@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title mb-0">Brokers</h3>
            </div>
        </div>
        <div class="content-body">
            <section id="configuration">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">

                            <div class="card-header">
                                <h4 class="card-title">Broker Information{{$brokers->name}}</h4>
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="row">


                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="card-text">
                                                        <dl>
                                                            <dt>Broker ID</dt>
                                                            <dd>{{$brokers->broker_id}}</dd>
                                                        </dl>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="card-text">
                                                        <dl>
                                                            <dt>Broker Name</dt>
                                                            <dd>{{$brokers->name}}</dd>
                                                        </dl>
                                                    </div>
                                                </div>


                                                <div class="col-md-4">
                                                    <div class="card-text">
                                                        <dl>
                                                            <dt>Phone Number</dt>
                                                            <dd>{{$brokers->phone_number}}</dd>
                                                        </dl>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="card-text">
                                                        <dl>
                                                            <dt>Email</dt>
                                                            <dd>{{$brokers->email}}</dd>
                                                        </dl>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="card-text">
                                                        <dl>
                                                            <dt>Location</dt>
                                                            <dd>{{$brokers->location}}</dd>
                                                        </dl>
                                                    </div>
                                                </div>


                                                <div class="col-md-4">
                                                    <div class="card-text">
                                                        <dl>
                                                            <dt>Address</dt>
                                                            <dd>{{$brokers->address}}</dd>
                                                        </dl>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="form-group row mb-0">
                                                <div class="col-md-12 offset-md-6">
                                                    <a href="{{URL::previous()}}" class="btn btn-secondary left">
                                                        {{ __('Back') }}
                                                    </a>
                                                </div>
                                            </div>

                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>


            </section>
        </div>
    </div>
@endsection
