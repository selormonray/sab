@extends('layouts.app')

@push("page-styles")
    <link rel="stylesheet" type="text/css" href={{asset('vendors/css/tables/datatable/datatables.min.css')}}>
@endpush

@section('content')
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title mb-0">Brokers</h3>
            </div>

            <div class="content-header-right col-md-6 col-12">
                <div class="media width-250 float-right">
                    <div class="media-body media-right text-right">
                        <button type="button" class="btn btn-float btn-primary"
                                style="padding: 8px 8px; width: 150px">
                            <a href="{{route('broker.add')}}" style="padding: 10px; color: #ffffff">ADD BROKERS</a>
                        </button>
                    </div>
                </div>
            </div>

        </div>
        <div class="content-body">
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">

                            <div class="card-header">
                                <h4 class="card-title">Showing All Brokers</h4>
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    </ul>
                                </div>
                            </div>



                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">


                                    @if (session('status'))
                                        <div class="alert {{ (session()->get("status.error")) ? "alert-danger" : "alert-success"}}" role="alert" style="margin-top: -25px; margin-bottom: 25px;">
                                            <strong>{{session()->get("status.title")}}</strong> {{session()->get("status.message")}}
                                        </div>
                                    @endif

                                    <table class="table table-striped table-bordered" id="broker_table">
                                        <thead>
                                        <tr>
                                            <th>Broker Name</th>
                                            <th>Phone Number</th>
                                            <th>Email</th>
                                            <th>Location</th>
                                            <th width="170px">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($broker as $broker)
                                            <tr>
                                                <td>{{$broker->name}}</td>
                                                <td>{{$broker->phone_number}}</td>
                                                <td>{{$broker->email}}</td>
                                                <td>{{$broker->location}}</td>
                                                <td>
                                                    <a href="{{route('broker.edit',[$broker->id])}}"
                                                       class="btn btn-secondary btn-sm">
                                                        <i class="icon-check"></i> Edit
                                                    </a>
                                                    <a href="{{route('broker.view', $broker->id)}}"
                                                       class="btn btn-success btn-sm">
                                                        <i class="icon-check"></i> View
                                                    </a>
                                                    <a class="dropdow-item btn btn-danger btn-sm delete"
                                                       href="{{ route('broker.delete',[$broker->id]) }}"
                                                       onclick="event.preventDefault();
                                                               if (confirm('Do you want to delete?')){
                                                               document.getElementById('delete-form-{{$broker->id}}').submit();
                                                               }
                                                               ">
                                                        {{ __('Delete') }}
                                                    </a>

                                                    <form id="delete-form-{{$broker->id}}"
                                                          action="{{ route('broker.delete',[$broker->id]) }}"
                                                          method="POST" style="display: none;">
                                                        @csrf {{method_field('DELETE')}}

                                                    </form>


                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection


@push("stack-script")
    <script>
        $(document).ready( function () {
            $('#broker_table').DataTable({
                // "scrollX": true
            });


        } );


    </script>
    <script src="{{asset('vendors/js/tables/datatable/datatables.min.js')}}" type="text/javascript"></script>
@endpush