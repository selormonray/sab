@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title mb-0">Brokers</h3>
            </div>
        </div>
        <div class="content-body">
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">

                            <div class="card-header">
                                <h4 class="card-title">Add Broker</h4>
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <p><h6>All fields marked <b>*</b> are required!</h6></p>
                                </div>
                            </div>

                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">

                                    @if (session('status'))
                                        <div class="alert {{ (session()->get("status.error")) ? "alert-danger" : "alert-success"}}"
                                             role="alert" style="margin-top: -25px; margin-bottom: 25px;">
                                            <strong>{{session()->get("status.title")}}</strong> {{session()->get("status.message")}}
                                        </div>
                                    @endif

                                    <form method="POST" action="{{ route('broker.save') }}">
                                        @csrf

                                        <div class="form-group row">
                                            <label for="broker_name" class="col-md-4 col-form-label text-md-right">{{ __('Broker Name *') }}</label>

                                            <div class="col-md-6">
                                                <input id="broker_name" type="text" class="form-control{{ $errors->has('broker_name') ? ' is-invalid' : '' }}" name="broker_name" value="{{ old('broker_name') }}" required autofocus>

                                                @if ($errors->has('broker_name'))
                                                    <span class="invalid-feedback" role="alert">
                                                         <strong>{{ $errors->first('broker_name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                            <div class="col-md-6">
                                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" >

                                                @if ($errors->has('email'))
                                                    <span class="invalid-feedback" role="alert">
                                                         <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <label for="phone_number" class="col-md-4 col-form-label text-md-right">{{ __('Phone Number ') }}</label>

                                            <div class="col-md-6">
                                                <input id="phone_number" type="phone_number" class="form-control{{ $errors->has('phone_number') ? ' is-invalid' : '' }}" name="phone_number" value="{{ old('phone_number') }}" >

                                                @if ($errors->has('phone_number'))
                                                    <span class="invalid-feedback" role="alert">
                                                         <strong>{{ $errors->first('phone_number') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="location" class="col-md-4 col-form-label text-md-right">{{ __('Location') }}</label>

                                            <div class="col-md-6">
                                                <input id="location" type="location" class="form-control{{ $errors->has('location') ? ' is-invalid' : '' }}" name="location" value="{{ old('location') }}" >

                                                @if ($errors->has('location'))
                                                    <span class="invalid-feedback" role="alert">
                                                         <strong>{{ $errors->first('location') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="address" class="col-md-4 col-form-label text-md-right">{{ __('Address') }}</label>

                                            <div class="col-md-6">
                                                <textarea id="address" type="address" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" value="{{ old('address') }}" ></textarea>

                                                @if ($errors->has('address'))
                                                    <span class="invalid-feedback" role="alert">
                                                         <strong>{{ $errors->first('address') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>




                                        <div class="form-group row mb-0">
                                            <div class="col-md-6 offset-md-4">
                                                <button type="submit" class="btn btn-primary mr-2">
                                                    {{ __('Add') }}
                                                </button>
                                                <a href="{{URL::previous()}}" class="btn btn-secondary left">
                                                    {{ __('Back') }}
                                                </a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection


