@extends('layouts.app')

@section('content')
    <div class="content-wrapper">

        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title mb-0">Users</h3>
            </div>
        </div>

        <div class="content-body">
            <section id="configuration">
                <div class="row">
                    <div class="col-12">

                        <div class="card">


                            <div class="card-header">
                                <h4 class="card-title">Set permissions for *{{$user->name}}</h4>
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        {{--<li><a data-action="expand"><i class="ft-maximize"></i></a></li>--}}
                                    </ul>
                                </div>
                            </div>

                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">


                                    @if (session('status'))
                                        <div class="alert {{ (session()->get("status.error")) ? "alert-danger" : "alert-success"}}"
                                             role="alert" style="margin-top: -25px; margin-bottom: 25px;">
                                            <strong>{{session()->get("status.title")}}</strong> {{session()->get("status.message")}}
                                        </div>
                                    @endif

                                    <form method="POST"
                                          action="{{ route('manage.user.permissions.save',[$user->id]) }}">
                                        @csrf
                                        <h6>Permissions</h6>
                                        <div class="form-group row">
                                            @foreach($permissions as $permission)
                                                <div class="col-md-3">
                                                    <div class="form-check mb-3">
                                                        <input class="form-check-input"
                                                               @if(in_array($permission->id,$user_permissions)) checked
                                                               @endif
                                                               type="checkbox" name="permissions[]"
                                                               value="{{$permission->id}}"
                                                               id="Check{{$permission->id}}">
                                                        <label class="form-check-label" for="Check{{$permission->id}}">
                                                            {{$permission->display_name}}
                                                        </label>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>

                                        <div class="form-group row mb-0">
                                            <div class="col-md-6 offset-md-4">
                                                <button type="submit" class="btn btn-primary mr-md-3">
                                                    {{ __('Save') }}
                                                </button>
                                                <a href="{{URL::previous()}}" class="btn btn-secondary left">
                                                    {{ __('Back') }}
                                                </a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </section>
        </div>
    </div>
@endsection
