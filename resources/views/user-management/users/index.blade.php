@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title mb-0">Users</h3>
            </div>

            <div class="content-header-right col-md-6 col-12">
                <div class="media width-250 float-right">
                    <div class="media-body media-right text-right">
                        <button type="button" class="btn btn-float btn-primary"
                                style="padding: 8px 8px; width: 150px">
                            <a href="{{route('manage.user.add')}}" style="padding: 10px; color: #ffffff">ADD USER</a>
                        </button>
                    </div>
                </div>
            </div>

        </div>
        <div class="content-body">
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">

                            <div class="card-header">
                                <h4 class="card-title">Showing All Users</h4>
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">


                                    @if (session('status'))
                                        <div class="alert {{ (session()->get("status.error")) ? "alert-danger" : "alert-success"}}" role="alert" style="margin-top: -25px; margin-bottom: 25px;">
                                            <strong>{{session()->get("status.title")}}</strong> {{session()->get("status.message")}}
                                        </div>
                                    @endif

                                    <table class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th scope="col">Name</th>
                                            <th scope="col">email</th>
                                            <th scope="col">Date Created</th>
                                            <th scope="col" width="230px">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($users as $user)
                                            <tr>
                                                <td>{{$user->name}}</td>
                                                <td>{{$user->email}}</td>
                                                <td>{{date('M d, Y',strtotime($user->created_at))}}</td>
                                                <td>
                                                    <a href="{{route('manage.user.edit',[$user->id])}}"
                                                       class="btn btn-secondary btn-sm">
                                                        <i class="icon-check"></i> Edit
                                                    </a>
                                                    <a class="dropdow-item btn btn-danger btn-sm"
                                                       href="{{ route('manage.user.delete',[$user->id]) }}"
                                                       onclick="event.preventDefault();
                                                     document.getElementById('delete-form').submit();">
                                                        {{ __('Delete') }}
                                                    </a>

                                                    <form id="delete-form"
                                                          action="{{ route('manage.user.delete',[$user->id]) }}"
                                                          method="POST" style="display: none;">
                                                        @csrf {{method_field('DELETE')}}
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    {{ $users->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
