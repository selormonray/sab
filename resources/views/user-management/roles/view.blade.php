@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title mb-0">Roles</h3>
            </div>
        </div>
        <div class="content-body">
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">

                            <div class="card-header">
                                <h4 class="card-title">Role details</h4>
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">


                                    @if (session('status'))
                                        <div class="alert {{ (session()->get("status.error")) ? "alert-danger" : "alert-success"}}"
                                             role="alert" style="margin-top: -25px; margin-bottom: 25px;">
                                            <strong>{{session()->get("status.title")}}</strong> {{session()->get("status.message")}}
                                        </div>
                                    @endif

                                    <p>
                                        Name : {{$role->name}}
                                    </p>
                                    <p>
                                        Display name : {{$role->display_name}}
                                    </p>
                                    <p>
                                        Description : {{$role->description}}
                                    </p>
                                    <p>List of permissions</p>

                                    {{--<ul class="list-group">--}}
                                    {{--@foreach($role->permissions as $permission)--}}
                                    {{--<a href="#"--}}
                                    {{--class="list-group-item list-group-item-action flex-column align-items-start">--}}
                                    {{--<div class="d-flex w-100 justify-content-between">--}}
                                    {{--<p class="mb-1">{{$permission->display_name}}</p>--}}
                                    {{--</div>--}}
                                    {{--<small>{{$permission->description}}</small>--}}
                                    {{--</a>--}}
                                    {{--@endforeach--}}
                                    {{--</ul>--}}


                                    <ul class="list-group">
                                        @foreach($role->permissions as $permission)
                                            <li class="list-group-item">{{$permission->display_name}}</li>
                                        @endforeach
                                    </ul>


                                    <br>

                                    <a style="margin-right: 20px" href="{{route('manage.role.edit',[$role->id])}}"
                                       class="btn btn-primary">Edit</a>

                                    <a href="{{route('manage.roles')}}" class="btn btn-secondary left">
                                        {{ __('Back') }}
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection

