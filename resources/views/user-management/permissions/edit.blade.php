@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title mb-0">Permissions</h3>
            </div>
        </div>
        <div class="content-body">
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">

                            <div class="card-header">
                                <h4 class="card-title">Edit Permission</h4>
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        {{--<li><a data-action="expand"><i class="ft-maximize"></i></a></li>--}}
                                    </ul>
                                </div>
                            </div>

                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">

                                    @if (session('status'))
                                        <div class="alert {{ (session()->get("status.error")) ? "alert-danger" : "alert-success"}}"
                                             role="alert" style="margin-top: -25px; margin-bottom: 25px;">
                                            <strong>{{session()->get("status.title")}}</strong> {{session()->get("status.message")}}
                                        </div>
                                    @endif

                                    <form method="POST"
                                          action="{{ route('manage.permission.update',[$permission->id]) }}">
                                        @csrf
                                        {{method_field('PUT')}}

                                        <div class="form-group row">
                                            <label for="name"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                            <div class="col-md-6">
                                                <input id="name" type="text"
                                                       class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                                       name="name"
                                                       value="{{ (old('name'))?old('name'):$permission->name }}"
                                                       required autofocus>

                                                @if ($errors->has('name'))
                                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="display_name"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('Display name') }}</label>

                                            <div class="col-md-6">
                                                <input id="display_name" type="text"
                                                       class="form-control{{ $errors->has('display_name') ? ' is-invalid' : '' }}"
                                                       name="display_name"
                                                       value="{{ (old('display_name'))?old('display_name'):$permission->display_name }}"
                                                       autofocus>

                                                @if ($errors->has('display_name'))
                                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('display_name') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="description"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>

                                            <div class="col-md-6">
                                                <input id="description" type="text"
                                                       class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}"
                                                       name="description"
                                                       value="{{ (old('description'))?old('description'):$permission->description }}">

                                                @if ($errors->has('description'))
                                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row mb-0">
                                            <div class="col-md-6 offset-md-4">
                                                <button type="submit" class="btn btn-primary mr-md-3">
                                                    {{ __('Update') }}
                                                </button>
                                                <a href="{{URL::previous()}}" class="btn btn-secondary left">
                                                    {{ __('Back') }}
                                                </a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection


