@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title mb-0">Claimants</h3>
            </div>
        </div>
        <div class="content-body">
            <section id="configuration">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">

                            <div class="card-header">
                                <h4 class="card-title">Claimant Information{{$claimant->name}}</h4>
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="row">

                                        <div class="col-md-6">
                                            <div class="card">
                                                <div class="text-center">
                                                    <div class="card-body">
                                                        <img src="{{asset('images/portrait/medium/avatar-m-1.png')}}" class="rounded-circle  height-150" alt="Card image">
                                                    </div>
                                                    <div class="card-body">
                                                        <h4 class="card-title"></h4>
                                                        {{--<h6 class="card-subtitle text-muted">Marketing Head</h6>--}}
                                                    </div>

                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-6">
                                            <div class="row">
                                            <div class="col-md-4">
                                                <div class="card-text">
                                                    <dl>
                                                        <dt>Name</dt>
                                                        <dd>{{$claimant->full_name}}</dd>
                                                    </dl>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="card-text">
                                                    <dl>
                                                        <dt>Date of Birth</dt>
                                                        <dd>{{date('d M, Y',strtotime($claimant->dob))}}</dd>
                                                    </dl>
                                                </div>
                                            </div>


                                            <div class="col-md-4">
                                                <div class="card-text">
                                                    <dl>
                                                        <dt>Gender</dt>
                                                        <dd>{{$claimant->gender}}</dd>
                                                    </dl>
                                                </div>
                                            </div>
                                            </div>


                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="card-text">
                                                        <dl>
                                                            <dt>Email</dt>
                                                            <dd>{{$claimant->email}}</dd>
                                                        </dl>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="card-text">
                                                        <dl>
                                                            <dt>Car Number</dt>
                                                            <dd>{{$claimant->car_number}}</dd>
                                                        </dl>
                                                    </div>
                                                </div>


                                                <div class="col-md-4">
                                                    <div class="card-text">
                                                        <dl>
                                                            <dt>Phone Number</dt>
                                                            <dd>{{$claimant->phone_number}}</dd>
                                                        </dl>
                                                    </div>
                                                </div>

                                            </div>


                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="card-text">
                                                        <dl>
                                                            <dt>Occupation</dt>
                                                            <dd>{{$claimant->occupation}}</dd>
                                                        </dl>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="card-text">
                                                        <dl>
                                                            <dt>Street Address</dt>
                                                            <dd>{{$claimant->street_address}}</dd>
                                                        </dl>
                                                    </div>
                                                </div>


                                                <div class="col-md-4">
                                                    <div class="card-text">
                                                        <dl>
                                                            <dt>Postal Address</dt>
                                                            <dd>{{$claimant->postal_address}}</dd>
                                                        </dl>
                                                    </div>
                                                </div>

                                            </div>



                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="card-text">
                                                        <dl>
                                                            <dt>Digital Address</dt>
                                                            <dd>{{$claimant->digital_address}}</dd>
                                                        </dl>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="card-text">
                                                        <dl>
                                                            <dt>Identification Type</dt>
                                                            <dd>
                                                                @if($claimant->identification_type == "drivers_license")
                                                                    {{"Drivers Licence"}}
                                                                @endif
                                                            </dd>
                                                        </dl>
                                                    </div>
                                                </div>


                                                <div class="col-md-4">
                                                    <div class="card-text">
                                                        <dl>
                                                            <dt>Identification Number</dt>
                                                            <dd>{{$claimant->identification_number}}</dd>
                                                        </dl>
                                                    </div>
                                                </div>

                                                <section id="hover-effects" class="card">

                                                <div class="col-md-6 col-12">
                                                    <figure class="effect-honey">
                                                        <img src="{{asset('images/gallery/4.jpg')}}" alt="img04">
                                                        <figcaption>

                                                            <a>Identification Card</a>
                                                        </figcaption>
                                                    </figure>
                                                </div>
                                                </section>

                                            </div>

                                            <div class="form-group row mb-0">
                                                <div class="col-md-12 offset-md-6">
                                                    <a href="{{URL::previous()}}" class="btn btn-secondary left">
                                                        {{ __('Back') }}
                                                    </a>
                                                </div>
                                            </div>


                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>


            </section>
        </div>
    </div>
@endsection
