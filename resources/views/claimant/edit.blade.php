@extends('layouts.app')

@section('content')
    @if($errors->any())
    <ul>
        @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
    @endif
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title mb-0">Edit Claimant</h3>
            </div>
        </div>
        <div class="content-body">
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">

                            <div class="card-header">
                                <h4 class="card-title">Edit Claimant</h4>
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <p><h6>All fields marked <b>*</b> are required!</h6></p>
                                </div>
                            </div>


                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">


                                    @if (session('status'))
                                        <div class="alert {{ (session()->get("status.error")) ? "alert-danger" : "alert-success"}}"
                                             role="alert" style="margin-top: -25px; margin-bottom: 25px;">
                                            <strong>{{session()->get("status.title")}}</strong> {{session()->get("status.message")}}
                                        </div>
                                    @endif

                                    <form method="POST" action="{{ route('claimant.update',[$claimant->id]) }}">
                                        @csrf {{method_field('PUT')}}

                                        <div class="form-group row">
                                            <label for="full_name"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('Full Name') }}</label>

                                            <div class="col-md-6">
                                                <input id="full_name" type="text"
                                                       class="form-control{{ $errors->has('full_name') ? ' is-invalid' : '' }}"
                                                       name="full_name" value="{{ (old('full_name'))?old('full_name'):$claimant->full_name }}" >

                                                @if ($errors->has('full_name'))
                                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('full_name') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>



                                        <div class="form-group row">
                                            <label for="phone_number"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('Phone Number *') }}</label>

                                            <div class="col-md-6">
                                                <input id="phone_number" type="text"
                                                       class="form-control{{ $errors->has('phone_number') ? ' is-invalid' : '' }}"
                                                       name="phone_number" value="{{ (old('phone_number'))?old('phone_number'):$claimant->phone_number }}"
                                                       required autofocus>

                                                @if ($errors->has('phone_number'))
                                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>



                                        <div class="form-group row">
                                            <label for="car_number"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('Car Number *') }}</label>

                                            <div class="col-md-6">
                                                <input id="car_number" type="car_number"
                                                       class="form-control{{ $errors->has('car_number') ? ' is-invalid' : '' }}"
                                                       name="car_number"
                                                       value="{{ (old('car_number'))?old('car_number'):$claimant->car_number }}"
                                                       required autofocus>

                                                @if ($errors->has('car_number'))
                                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('car_number') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>


                                        {{--<div class="form-group row">--}}
                                            {{--<label for="street_address"--}}
                                                   {{--class="col-md-4 col-form-label text-md-right">{{ __('Street Address') }}</label>--}}

                                            {{--<div class="col-md-6">--}}
                                                {{--<input id="street_address" type="street_address"--}}
                                                       {{--class="form-control{{ $errors->has('street_address') ? ' is-invalid' : '' }}"--}}
                                                       {{--name="street_address"--}}
                                                       {{--value="{{ (old('street_address'))?old('location'):$claimant->street_address }}"--}}
                                                       {{--required>--}}

                                                {{--@if ($errors->has('street_address'))--}}
                                                    {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $errors->first('street_address') }}</strong>--}}
                                    {{--</span>--}}
                                                {{--@endif--}}
                                            {{--</div>--}}
                                        {{--</div>--}}


                                        {{--<div class="form-group row">--}}
                                            {{--<label for="postal_address"--}}
                                                   {{--class="col-md-4 col-form-label text-md-right">{{ __('Postal Address') }}</label>--}}

                                            {{--<div class="col-md-6">--}}
                                                {{--<input id="postal_address" type="postal_address"--}}
                                                       {{--class="form-control{{ $errors->has('postal_address') ? ' is-invalid' : '' }}"--}}
                                                       {{--name="postal_address"--}}
                                                       {{--value="{{ (old('postal_address'))?old('postal_address'):$claimant->postal_address }}"--}}
                                                       {{--required>--}}

                                                {{--@if ($errors->has('postal_address'))--}}
                                                    {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $errors->first('postal_address') }}</strong>--}}
                                    {{--</span>--}}
                                                {{--@endif--}}
                                            {{--</div>--}}
                                        {{--</div>--}}

                                        <div class="form-group row mb-0">
                                            <div class="col-md-6 offset-md-4">
                                                <button type="submit" class="btn btn-primary mr-md-3">
                                                    {{ __('Save') }}
                                                </button>
                                                <a href="{{URL::previous()}}" class="btn btn-secondary left">
                                                    {{ __('Back') }}
                                                </a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection

