<script src="{{ asset('vendors/js/extensions/toastr.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {

        var message = "{{ session()->get("notification.message", "") }}".replace(/@@@/g , "<br><br>");
        var type = "{{ session()->get("notification.type", "info") }}";
        var title = "{{ session()->get("notification.title", "") }}";

            toastr[type](message, title, {
                positionClass: 'toast-top-{{ session()->get("notification.position", "right") }}',
                "showMethod": "slideDown",
                "hideMethod": "slideUp",
                timeOut: 8000
            });

    });

</script>

