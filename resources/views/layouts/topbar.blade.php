<!-- fixed-top-->
<nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-static-top navbar-dark bg-gradient-x-grey-blue navbar-border navbar-brand-center">
    <div class="navbar-wrapper">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mobile-menu d-md-none mr-auto"><a href="#"
                                                                      class="nav-link nav-menu-main menu-toggle hidden-xs"><i
                                class="ft-menu font-large-1"></i></a></li>
                <li class="nav-item">
                    <a href="{{ url('/') }}" class="navbar-brand">
                        <h2 class="brand-text" style="padding-left: 0px">
                            {{--<img alt="aginqua logo" src="" class="brand-logo">--}}
                            ICM
                        </h2>
                    </a>
                </li>
                <li class="nav-item d-md-none">
                    <a data-toggle="collapse" data-target="#navbar-mobile" class="nav-link open-navbar-container">
                        <i class="fa fa-ellipsis-v"></i></a>
                </li>
            </ul>
        </div>
        <div class="navbar-container container center-layout">
            <div id="navbar-mobile" class="collapse navbar-collapse">

                <ul class="nav navbar-nav mr-auto float-left">
                    <li class="nav-item d-none d-md-block"><a href="#" class="nav-link nav-link-expand"><i
                                    class="ficon ft-maximize"></i></a></li>
                </ul>

                <ul class="nav navbar-nav float-right">
                    <li class="dropdown dropdown-notification nav-item">
                        <a href="#" data-toggle="dropdown" class="nav-link nav-link-label"><i class="ficon ft-bell"></i>
                            <span class="badge badge-pill badge-default badge-danger badge-default badge-up">0</span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                            <li class="dropdown-menu-header">
                                <h6 class="dropdown-header m-0">
                                    <span class="grey darken-2">Notifications</span>
                                    <span class="notification-tag badge badge-default badge-danger float-right m-0">0 New</span>
                                </h6>
                            </li>
                            <li class="scrollable-container media-list">
                                {{--<a href="javascript:void(0)">--}}
                                {{--<div class="media">--}}
                                {{--<div class="media-left align-self-center"><i class="ft-file icon-bg-circle bg-teal"></i></div>--}}
                                {{--<div class="media-body">--}}
                                {{--<h6 class="media-heading">Generate monthly report</h6>--}}
                                {{--<small>--}}
                                {{--<time datetime="2015-06-11T18:29:20+08:00" class="media-meta text-muted">Last month</time>--}}
                                {{--</small>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</a>--}}
                            </li>
                            <li class="dropdown-menu-footer"><a href="javascript:void(0)"
                                                                class="dropdown-item text-muted text-center">No
                                    notifications found</a></li>
                        </ul>
                    </li>

                    <li class="dropdown dropdown-user nav-item">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link dropdown-user-link">
                            <span class="avatar avatar-online">
                                 <img src="{{ asset('images/portrait/small/avatar-s-1.png') }}" alt="avatar"><i></i>
                            </span>

                            @php
                                if(!empty(Auth::user())) {
                                    $split_name = explode(" ", Auth::user()->name, 2);
                                    $first_name = ucwords($split_name[0]);
                                    $last_name = "";
                                    if (!empty($split_name[1])) {
                                        $last_name = substr( ucwords($split_name[1]), 0,3).".";
                                    }
                                }
                            @endphp

                            <span class="user-name">{{$first_name." ".$last_name}}</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right"><a href="#" class="dropdown-item"><i
                                        class="ft-user"></i> Edit Profile</a>
                            <a href="#" class="dropdown-item"><i class="ft-check-square"></i> Coming Soon</a>
                            <div class="dropdown-divider"></div>

                            <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                <i class="ft-power"></i> {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>

                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>


