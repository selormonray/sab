<!-- Horizontal navigation-->
<div role="navigation" data-menu="menu-wrapper" class="header-navbar navbar-expand-sm navbar navbar-horizontal navbar-fixed navbar-light navbar-without-dd-arrow navbar-shadow menu-border">
    <!-- Horizontal menu content-->
    <div data-menu="menu-container" class="navbar-container main-menu-content container center-layout">
        <ul id="main-menu-navigation" data-menu="menu-navigation" class="nav navbar-nav">
            <li data-menu="dropdown" class="dropdown nav-item" style="margin-right: 10px">
                <a href="{{ route('home') }}" class="nav-link"><i class="ft-home"></i>
                    <span>Dashboard</span>
                </a>
                <ul class="dropdown-menu">
                    <li data-menu="" class=""><a href="{{ url('/') }}">Dashboard</a></li>
                </ul>
            </li>
            <li data-menu="dropdown" class="dropdown nav-item" style="margin-right: 10px">
                <a href="{{route('claimants')}}" class="nav-link"><i class="ft-users"></i>
                    <span>Claimants</span>
                </a>
            </li>
            <li data-menu="dropdown" class="dropdown nav-item" style="margin-right: 10px">
                <a href="{{route('claims')}}" class="nav-link"><i class="ft-file-text"></i>
                    <span>Claims</span>
                </a>
            </li>
            <li data-menu="dropdown" class="dropdown nav-item" style="margin-right: 10px">
                <a href="{{route('brokers')}}" class="nav-link"><i class="ft-users"></i>
                    <span>Brokers</span>
                </a>
            </li>

            <li data-menu="dropdown" class="dropdown nav-item" style="margin-right: 10px">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link"><i class="ft-user-check"></i>
                    <span>User Management</span>
                </a>
                <ul class="dropdown-menu">
                    <li data-menu="" class=""><a href="{{route('manage.users')}}" data-toggle="dropdown" class="dropdown-item">System Users</a>
                    </li>
                    <li data-menu="" class=""><a href="{{route('manage.roles')}}" data-toggle="dropdown" class="dropdown-item">Roles</a>
                    </li>
                    <li data-menu="" class=""><a href="{{route('manage.permissions')}}" data-toggle="dropdown" class="dropdown-item">Permissions</a>
                    </li>
                </ul>
            </li>

            <li data-menu="dropdown" class="dropdown nav-item">
                <a href="{{ url('/') }}" data-toggle="dropdown" class="dropdown-toggle nav-link"><i class="ft-settings"></i>
                    <span>System Settings</span>
                </a>
                <ul class="dropdown-menu">
                    <li data-menu="" class=""><a href="" data-toggle="dropdown" class="dropdown-item">Coming Soon</a>
                    </li>
                    <li data-menu=""><a href="" data-toggle="dropdown" class="dropdown-item">Coming Soon</a>
                    </li>
                    <li data-menu=""><a href="" data-toggle="dropdown" class="dropdown-item">Coming Soon</a>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
    <!-- /horizontal menu content-->
</div>
<!-- Horizontal navigation-->