<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" data-textdirection="ltr" class="loading">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Aginqua Insurance Management System">
    <meta name="keywords" content="africa, smart, insurance, travel insurance">
    <meta name="author" content="Aginqua">

    <title>{{ config('app.name', 'Aginqua') }} - Africa's Smart Travel Insurance</title>

    <link rel="apple-touch-icon" href="{{ asset('images/logo/a_aginqua_80x80.png') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/logo/a_aginqua_80x80.png') }}">

    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i"
          rel="stylesheet">

    <link rel="stylesheet" type="text/css" href={{asset('css/tables/datatable/datatables.min.css')}}>


    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/vendors.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/forms/icheck/icheck.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/forms/icheck/custom.css')}}">
    <!-- END VENDOR CSS-->

    <!-- BEGIN STACK CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
    <!-- END STACK CSS-->

    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/core/menu/menu-types/vertical-menu-modern.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/core/colors/palette-gradient.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/pages/login-register.css')}}">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
@section('styles')
@show
<!-- END Custom CSS-->
</head>
<body class="horizontal-layout horizontal-menu 1-column   menu-expanded blank-page blank-page"
      data-open="click" data-menu="horizontal-menu" data-col="1-column">





<div class="app-content container center-layout mt-2">
    <div class="content-wrapper">

        @yield('content')

    </div>
</div>

















<!-- ////////////////////////////////////////////////////////////////////////////-->
<!-- BEGIN VENDOR JS-->
<script src="{{asset('vendors/js/vendors.min.js')}}" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="{{asset('vendors/js/forms/icheck/icheck.min.js')}}" type="text/javascript"></script>
<script src="{{asset('vendors/js/forms/validation/jqBootstrapValidation.js')}}"
        type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<script src={{asset('js/tables/datatable/datatables.min.js')}} type="text/javascript"></script>

<script src={{asset('js/scripts/tables/datatables/datatable-basic.js')}}
        type="text/javascript"></script>


<!-- BEGIN STACK JS-->
<script src="{{asset('js/core/app-menu.js')}}" type="text/javascript"></script>
<script src="{{asset('js/core/app.js')}}" type="text/javascript"></script>
<script src="{{asset('js/scripts/customizer.js')}}" type="text/javascript"></script>
<!-- END STACK JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="{{asset('js/scripts/forms/form-login-register.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->

@stack('plugins')

</body>
</html>