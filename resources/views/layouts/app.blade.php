<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" data-textdirection="ltr" class="loading">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Aginqua Insurance Management System">
    <meta name="keywords" content="africa, smart, insurance, travel insurance">
    <meta name="author" content="Aginqua">

    <title>{{ config('app.name', 'ICM') }} - Insurance Claims Management System</title>

    <link rel="apple-touch-icon" href="">
    <link rel="shortcut icon" type="image/x-icon" href="">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i"
          rel="stylesheet">

    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/vendors.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/css/extensions/unslider.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/css/weather-icons/climacons.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('fonts/meteocons/style.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/css/charts/morris.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/extensions/toastr.css')}}">
    @stack('vendor-styles')

    <!-- END VENDOR CSS-->

    <!-- BEGIN LARAVEL DEFAULT CSS-->
    {{--<link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">--}}
    <!-- END LARAVEL DEFAULT CSS-->

    <!-- BEGIN STACK CSS-->
    @stack('stack-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('css/plugins/extensions/toastr.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/core/colors/palette-tooltip.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
    <!-- END STACK CSS-->






    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/core/menu/menu-types/horizontal-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('fonts/simple-line-icons/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/core/colors/palette-gradient.css') }}">
    <link rel="stylesheet" type="text/css" href=" {{ asset('css/pages/timeline.css') }}">
    @stack('page-styles')
    <!-- END Page Level CSS-->

    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('custom-assets/css/style.css') }}">
    <!-- END Custom CSS-->

</head>
<body data-open="click" data-menu="horizontal-menu" data-col="2-columns" class="horizontal-layout horizontal-menu 2-columns   menu-expanded">

<!-- fixed-top -->
@include('layouts.topbar')
<!-- end fixed-top -->


<!-- Horizontal navigation-->
@include('layouts.menubar')
<!-- Horizontal navigation-->


<main class="app-content container center-layout mt-2">
    <div class="content-wrapper" style="min-height: 600px">
        @yield('content')
    </div>
</main>


@include('layouts.footer')

<!-- BEGIN LARAVEL DEFAULT JS -->
{{--<script src="{{ asset('js/app.js') }}" defer></script>--}}
<!-- END LARAVEL DEFAULT JS-->

<!-- BEGIN VENDOR JS-->
<script src="{{ asset('vendors/js/vendors.min.js') }}" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->

<!-- BEGIN PAGE VENDOR JS-->
<script type="text/javascript" src="{{ asset('vendors/js/ui/jquery.sticky.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendors/js/charts/jquery.sparkline.min.js') }}"></script>
<script src="{{ asset('vendors/js/charts/raphael-min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/js/charts/morris.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/js/extensions/unslider-min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/js/timeline/horizontal-timeline.js')}}" type="text/javascript"></script>
@stack('vendor-script')
<!-- END PAGE VENDOR JS-->

<!-- BEGIN STACK JS-->
<script src="{{ asset('js/core/app-menu.js')}}" type="text/javascript"></script>
<script src="{{ asset('js/core/app.js')}}" type="text/javascript"></script>
<!-- END STACK JS-->


<!-- BEGIN PAGE LEVEL JS-->
<script type="text/javascript" src="{{ asset('js/scripts/ui/breadcrumbs-with-stats.js')}}"></script>
<script src="{{ asset('js/scripts/pages/dashboard-ecommerce.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->
@stack('stack-script')


@stack('plugins')


@if(session()->has('notification'))
    @component('layouts.toast-notification')
    @endcomponent
@endif

</body>
</html>