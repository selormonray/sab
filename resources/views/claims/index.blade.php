@extends('layouts.app')

@push("page-styles")
    <link rel="stylesheet" type="text/css" href={{asset('vendors/css/tables/datatable/datatables.min.css')}}>
@endpush

@section('content')
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title mb-0">Claims</h3>
            </div>

            <div class="content-header-right col-md-6 col-12">
                <div class="media width-250 float-right">
                    <div class="media-body media-right text-right">
                        {{--<button type="button" class="btn btn-float btn-primary"--}}
                        {{--style="padding: 8px 8px; width: 150px">--}}
                        {{--<a href="{{route('claimant.add')}}" style="padding: 10px; color: #ffffff">ADD CLAIMANTS</a>--}}
                        {{--</button>--}}
                    </div>
                </div>
            </div>

        </div>
        <div class="content-body">
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">

                            <div class="card-header">
                                <h4 class="card-title">Showing All Claims</h4>
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    </ul>
                                </div>
                            </div>


                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">


                                    @if (session('status'))
                                        <div class="alert {{ (session()->get("status.error")) ? "alert-danger" : "alert-success"}}"
                                             role="alert" style="margin-top: -25px; margin-bottom: 25px;">
                                            <strong>{{session()->get("status.title")}}</strong> {{session()->get("status.message")}}
                                        </div>
                                    @endif

                                    <table class="table table-striped table-bordered" id="claims_table">
                                        <thead>
                                        <tr>
                                            <th>Car Number</th>
                                            <th>Broker</th>
                                            <th>Claim Type</th>
                                            <th>Police Info</th>
                                            <th>Status</th>
                                            <th>Action</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($claims as $claim)
                                            <tr>
                                                <td><a href="{{route('claims.view', $claim->id)}}">{{$claim->claimant->car_number	}}</a></td>
                                                <td>{{$claim->broker->name}}</td>
                                                <td>{{$claim->claim_type}}</td>
                                                <td>
                                                    {{$claim->policeInfo->station_name}}
                                                    {{$claim->policeInfo->location}}
                                                </td>
                                                <td>
                                                    @if($claim->status == "declined")
                                                        <div class="badge badge-pill badge-danger">Declined</div>
                                                    @elseif($claim->status == "approved")
                                                        <div class="badge badge-pill badge-success">Approved</div>
                                                    @elseif($claim->status == "processing")
                                                        <div class="badge badge-pill badge-info">Processing</div>
                                                    @else
                                                        <div class="badge badge-pill badge-primary">Pending</div>
                                                    @endif

                                                </td>
                                                <td>
                                                    <div class="btn-group" role="group" aria-label="Basic example">
                                                        {{--Processing Button--}}
                                                        <div class="btn-group" role="group"
                                                             aria-label="Basic example">
                                                            <button type="button" class="btn btn-primary"
                                                                    onclick="document.getElementById('processing{{$claim->id}}').submit()
                                                                            event.preventDefault();"
                                                                    @if($claim->status == "processing")disabled="disabled"@endif>
                                                                Start Processing
                                                            </button>

                                                            <form id="processing{{$claim->id}}"
                                                                  action="{{route('process-claim')}}" method="POST"
                                                                  style="display: none">
                                                                @csrf
                                                                <input name="id" type="hidden"
                                                                       value="{{$claim->id}}">
                                                            </form>

                                                            {{--Approving Button--}}

                                                            <button type="button" class="btn btn-success"
                                                                    onclick="document.getElementById('approving{{$claim->id}}').submit()
                                                                            event.preventDefault();"
                                                                    @if($claim->status == "approved")disabled="disabled"@endif>
                                                                Approve
                                                            </button>
                                                            <form id="approving{{$claim->id}}"
                                                                  action="{{route('approve-claim')}}"
                                                                  method="post" style="display: none">
                                                                @csrf
                                                                <input name="id" type="hidden"
                                                                       value="{{$claim->id}}">
                                                            </form>


                                                            {{--Decline Claim--}}

                                                            <button type="button" class="btn btn-warning"
                                                                    onclick="document.getElementById('declining{{$claim->id}}').submit();
                                                                            event.preventDefault();"
                                                                    @if($claim->status == "declined")disabled="disabled"@endif>
                                                                Decline
                                                            </button>

                                                            <form id="declining{{$claim->id}}"
                                                                  action="{{route('decline-claim')}}"
                                                                  method="post" style="display: none">
                                                                @csrf
                                                                <input name="id" type="hidden"
                                                                       value="{{$claim->id}}">
                                                            </form>
                                                        </div>


                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection


@push("stack-script")

    <script src="{{asset('vendors/js/tables/datatable/datatables.min.js')}}" type="text/javascript"></script>
@endpush