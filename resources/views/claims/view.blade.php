@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title mb-0">Claim</h3>
            </div>
        </div>
        <div class="content-body">
            <section id="configuration">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">

                            <div class="card-header">
                                {{--<h4 class="card-title">Claimant Information{{$claim->name}}</h4>--}}
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="card-text">
                                                        <dl>
                                                            <dt>Claim ID</dt>
                                                            <dd>{{$claim->claim_id}}</dd>
                                                        </dl>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="card-text">
                                                        <dl>
                                                            <dt>Claimant ID</dt>
                                                            <dd>{{$claim->id}}</dd>
                                                        </dl>
                                                    </div>
                                                </div>


                                                <div class="col-md-4">
                                                    <div class="card-text">
                                                        <dl>
                                                            <dt>Claim Type</dt>
                                                            <dd>{{$claim->claim_type}}</dd>
                                                        </dl>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="card-text">
                                                        <dl>
                                                            <dt>Policy Number</dt>
                                                            <dd>{{$claim->policy_number}}</dd>
                                                        </dl>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="card-text">
                                                        <dl>
                                                            <dt>Broker ID</dt>
                                                            <dd>{{$claim->broker_id}}</dd>
                                                        </dl>
                                                    </div>
                                                </div>


                                                <div class="col-md-4">
                                                    <div class="card-text">
                                                        <dl>
                                                            <dt>Police Report</dt>
                                                            <dd>{{$claim->police_report}}</dd>
                                                        </dl>
                                                    </div>
                                                </div>

                                            </div>


                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="card-text">
                                                        <dl>
                                                            <dt>Details</dt>
                                                            <dd>{{$claim->details}}</dd>
                                                        </dl>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="card-text">
                                                        <dl>
                                                            <dt>Status</dt>
                                                            <dd>{{$claim->status}}</dd>
                                                        </dl>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="form-group row mb-0">
                                                <div class="col-md-12 offset-md-6">
                                                    <a href="{{URL::previous()}}" class="btn btn-secondary left">
                                                        {{ __('Back') }}
                                                    </a>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>


            </section>
        </div>
    </div>
@endsection
